import Foundation
import CoreData
import XCTest
@testable import Gitter

class JsonToDatabaseTests: XCTestCase {

    private var managedObjectContext: NSManagedObjectContext?
    private var jsonToDatabase: JsonToDatabase?

    private let test_room_json = [
        "id": "test_room",
        "security": "PUBLIC",
        "avatarUrl": "https://avatars-01.gitter.im/group/i/57542c12c43b8c601976fa66",
        "userCount": 3,
        "mentions": 0,
        "activity": false,
        "uri": "trevorah-test/test-repo",
        "roomMember": true,
        "topic": "i am a test repo",
        "unreadItems": 0,
        "lastAccessTime": "2016-05-17T19:52:43.976Z",
        "name": "trevorah-test/test-repo",
        "tags": ["test"],
        "githubType": "REPO",
        "lurk": true,
        "oneToOne": false,
        "noindex": false,
        "url": "/trevorah-test/test-repo"
    ] as [String : Any]

    private let test_fav_1_json = [
        "id": "test_fav_1",
        "security": "PUBLIC",
        "avatarUrl": "https://avatars-01.gitter.im/group/i/57542c12c43b8c601976fa66",
        "userCount": 3,
        "mentions": 0,
        "activity": false,
        "favourite": 1,
        "uri": "trevorah-test/test-repo",
        "roomMember": true,
        "topic": "i am a test repo",
        "unreadItems": 0,
        "lastAccessTime": "2016-05-17T19:52:43.976Z",
        "name": "trevorah-test/test-repo",
        "tags": ["test"],
        "githubType": "REPO",
        "lurk": true,
        "oneToOne": false,
        "noindex": false,
        "url": "/trevorah-test/test-repo"
    ] as [String : Any]

    private let test_fav_2_json = [
        "id": "test_fav_2",
        "security": "PUBLIC",
        "avatarUrl": "https://avatars-01.gitter.im/group/i/57542c12c43b8c601976fa66",
        "userCount": 3,
        "mentions": 0,
        "activity": false,
        "favourite": 2,
        "uri": "trevorah-test/test-repo",
        "roomMember": true,
        "topic": "i am a test repo",
        "unreadItems": 0,
        "lastAccessTime": "2016-05-17T19:52:43.976Z",
        "name": "trevorah-test/test-repo",
        "tags": ["test"],
        "githubType": "REPO",
        "lurk": true,
        "oneToOne": false,
        "noindex": false,
        "url": "/trevorah-test/test-repo"
    ] as [String : Any]

    private let test_fav_3_json = [
        "id": "test_fav_3",
        "security": "PUBLIC",
        "avatarUrl": "https://avatars-01.gitter.im/group/i/57542c12c43b8c601976fa66",
        "userCount": 3,
        "mentions": 0,
        "activity": false,
        "favourite": 3,
        "uri": "trevorah-test/test-repo",
        "roomMember": true,
        "topic": "i am a test repo",
        "unreadItems": 0,
        "lastAccessTime": "2016-05-17T19:52:43.976Z",
        "name": "trevorah-test/test-repo",
        "tags": ["test"],
        "githubType": "REPO",
        "lurk": true,
        "oneToOne": false,
        "noindex": false,
        "url": "/trevorah-test/test-repo"
    ] as [String : Any]

    private let test_lurk_room_json = [
        "id": "test_lurk_room",
        "security": "PUBLIC",
        "avatarUrl": "https://avatars-01.gitter.im/group/i/57542c12c43b8c601976fa66",
        "userCount": 3,
        "mentions": 0,
        "activity": false,
        "uri": "trevorah-test/test-repo",
        "roomMember": true,
        "topic": "i am a test repo",
        "unreadItems": 0,
        "lastAccessTime": "2016-05-17T19:52:43.976Z",
        "name": "trevorah-test/test-repo",
        "tags": ["test"],
        "githubType": "REPO",
        "lurk": true,
        "oneToOne": false,
        "noindex": false,
        "url": "/trevorah-test/test-repo"
    ] as [String : Any]

    private let test_lurk_room_with_activity_json = [
        "id": "test_lurk_room_with_activity",
        "security": "PUBLIC",
        "avatarUrl": "https://avatars-01.gitter.im/group/i/57542c12c43b8c601976fa66",
        "userCount": 3,
        "mentions": 0,
        "activity": true,
        "uri": "trevorah-test/test-repo",
        "roomMember": true,
        "topic": "i am a test repo",
        "unreadItems": 0,
        "lastAccessTime": "2016-05-17T19:52:43.976Z",
        "name": "trevorah-test/test-repo",
        "tags": ["test"],
        "githubType": "REPO",
        "lurk": true,
        "oneToOne": false,
        "noindex": false,
        "url": "/trevorah-test/test-repo"
    ] as [String : Any]

    private let test_room_without_unreads_json = [
        "id": "test_room_unreads",
        "security": "PUBLIC",
        "avatarUrl": "https://avatars-01.gitter.im/group/i/57542c12c43b8c601976fa66",
        "activity": false,
        "uri": "trevorah-test/test-repo",
        "roomMember": true,
        "topic": "i am a test repo",
        "userCount": 3,
        "lastAccessTime": "2016-05-17T19:52:43.976Z",
        "name": "trevorah-test/test-repo",
        "tags": ["test"],
        "githubType": "REPO",
        "lurk": true,
        "oneToOne": false,
        "noindex": false,
        "url": "/trevorah-test/test-repo"
        ] as [String : Any]

    private let test_room_with_unreads_json = [
        "id": "test_room_unreads",
        "security": "PUBLIC",
        "avatarUrl": "https://avatars-01.gitter.im/group/i/57542c12c43b8c601976fa66",
        "activity": false,
        "uri": "trevorah-test/test-repo",
        "roomMember": true,
        "topic": "i am a test repo",
        "userCount": 3,
        "mentions": 1,
        "unreadItems": 2,
        "lastAccessTime": "2016-05-17T19:52:43.976Z",
        "name": "trevorah-test/test-repo",
        "tags": ["test"],
        "githubType": "REPO",
        "lurk": true,
        "oneToOne": false,
        "noindex": false,
        "url": "/trevorah-test/test-repo"
        ] as [String : Any]

    override func setUp() {
        super.setUp()

        let modelURL = Bundle.main.url(forResource: "Gitter", withExtension: "momd")!
        let model = NSManagedObjectModel(contentsOf: modelURL)!
        let coordinator = NSPersistentStoreCoordinator(managedObjectModel: model)
        try! coordinator.addPersistentStore(ofType: NSInMemoryStoreType, configurationName: nil, at: nil, options: nil)

        managedObjectContext = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
        managedObjectContext!.persistentStoreCoordinator = coordinator
        jsonToDatabase = JsonToDatabase(context: managedObjectContext!)
    }

    override func tearDown() {
        super.tearDown()

        // attempt to save the context to ensure that it is still valid
        try! managedObjectContext?.save()
    }

    func testUpsertDoesntAddToRoomList() throws {
        try jsonToDatabase!.upsertRoom(test_room_json)

        let room = getRoomFromDb("test_room")
        XCTAssertNil(room!.userRoomCollection)
    }

    func testRoomListUpsertAddsToRoomList() throws {
        try jsonToDatabase!.upsertRoomInUserRoomList(test_room_json)

        let room = getRoomFromDb("test_room")
        XCTAssertNotNil(room!.userRoomCollection)
    }

    func testUpdateRoomListSetsCorrectProperty() throws {
        try jsonToDatabase!.updateUserRoomList([test_room_json])
        let room = getRoomFromDb("test_room")

        XCTAssertNotNil(room!.userRoomCollection)
    }

    func testPatchingMissingRoomDoesNotCreateRoom() throws {
        // assuming that the db is empty
        try jsonToDatabase!.patchRoom(test_room_json)

        let room = self.getRoomFromDb("test_room")
        XCTAssertNil(room)
    }

    func testNonFavouriteRoomHasCorrectComparable() throws {
        try jsonToDatabase!.upsertRoom(test_room_json)

        let room = self.getRoomFromDb("test_room")
        XCTAssertEqual(room!.favourite, 0)
        XCTAssertEqual(room!.favourite_comparable, Int32.max)
    }

    func testPatchingRoomToFavourite() throws {
        try jsonToDatabase!.upsertRoom(test_room_json)
        try jsonToDatabase!.patchRoom(["id": "test_room", "favourite": 1])

        let room = self.getRoomFromDb("test_room")
        XCTAssertEqual(room!.favourite, 1)
        XCTAssertEqual(room!.favourite_comparable, 1)
    }

    func testPatchingRoomToUnFavourite() throws {
        try jsonToDatabase!.upsertRoom(test_fav_1_json)
        try jsonToDatabase!.patchRoom(["id": "test_fav_1", "favourite": NSNull()])

        let room = self.getRoomFromDb("test_fav_1")
        XCTAssertEqual(room!.favourite, 0)
        XCTAssertEqual(room!.favourite_comparable, Int32.max)
    }

    func testFavouriteOrdering() throws {
        let roomListJson = [test_room_json, test_fav_1_json, test_fav_2_json, test_fav_3_json] as [JsonObject]

        try jsonToDatabase!.updateUserRoomList(roomListJson)

        let sortedRooms = self.getSortedRoomList()
        let ids = sortedRooms.map({ (room) -> String in
            return room.id
        })
        let favs = sortedRooms.map({ (room) -> Int32 in
            return room.favourite
        })
        let comps = sortedRooms.map({ (room) -> Int32 in
            return room.favourite_comparable
        })
        XCTAssertEqual(["test_fav_1", "test_fav_2", "test_fav_3", "test_room"], ids)
        XCTAssertEqual([1, 2, 3, 0], favs)
        XCTAssertEqual([1, 2, 3, Int32.max], comps)
    }

    func testFavouriteClashesCorrectThemselves() throws {
        let roomListJson = [test_room_json, test_fav_1_json, test_fav_2_json, test_fav_3_json] as [JsonObject]

        try jsonToDatabase!.updateUserRoomList(roomListJson)
        try jsonToDatabase!.patchRoom(["id": "test_room", "favourite": 1])

        let sortedRooms = self.getSortedRoomList()
        let ids = sortedRooms.map({ (room) -> String in
            return room.id
        })
        let favs = sortedRooms.map({ (room) -> Int32 in
            return room.favourite
        })
        let comps = sortedRooms.map({ (room) -> Int32 in
            return room.favourite_comparable
        })
        XCTAssertEqual(["test_room", "test_fav_1", "test_fav_2", "test_fav_3"], ids)
        XCTAssertEqual([1, 2, 3, 4], favs)
        XCTAssertEqual([1, 2, 3, 4], comps)
    }

    func testLurkingRoomShowsActivityWhenPatched() throws {
        try jsonToDatabase!.upsertRoomInUserRoomList(test_lurk_room_json)
        // just to check that the test json hasnt been messed with
        XCTAssertFalse(getRoomFromDb("test_lurk_room")!.activity)

        try jsonToDatabase!.patchRoom(["id": "test_lurk_room", "activity": true])

        XCTAssertTrue(getRoomFromDb("test_lurk_room")!.activity)
    }

    func testLurkingRoomClearsActivityCorrectly() throws {
        try jsonToDatabase!.upsertRoomInUserRoomList(test_lurk_room_with_activity_json)
        // just to check that the test json hasnt been messed with
        XCTAssertTrue(getRoomFromDb("test_lurk_room_with_activity")!.activity)

        try jsonToDatabase!.patchRoom(["id": "test_lurk_room_with_activity", "lastAccessTime": "2016-05-20T19:52:43.976Z"])

        XCTAssertFalse(getRoomFromDb("test_lurk_room_with_activity")!.activity)
    }

    func testRoomCreatedWithoutUnreadsDefaultsToZero() throws {
        // just to check that the test json hasnt been messed with
        XCTAssertEqual(test_room_without_unreads_json["unreadItems"] as! Int?, nil)

        try jsonToDatabase!.upsertRoomInUserRoomList(test_room_without_unreads_json)

        XCTAssertEqual(getRoomFromDb("test_room_unreads")!.unreadItems, 0)
    }

    func testRoomUpdateWithoutUnreadsDoesntOverwriteUnreads() throws {
        try jsonToDatabase!.upsertRoomInUserRoomList(test_room_with_unreads_json)
        XCTAssertEqual(getRoomFromDb("test_room_unreads")!.unreadItems, 2)

        try jsonToDatabase!.upsertRoomInUserRoomList(test_room_without_unreads_json)

        XCTAssertEqual(getRoomFromDb("test_room_unreads")!.unreadItems, 2)
    }

    func testLeaveRoomRemovesFromList() throws {
        try jsonToDatabase!.updateUserRoomList([test_room_json])

        // remove then patch is a typical server message when leaving a room
        try jsonToDatabase!.removeRoomFromUserRoomList(["id": "test_room"])
        try jsonToDatabase!.patchRoom(["id": "test_room", "mentions": 0, "unreadItems": 0])

        XCTAssertNil(getRoomFromDb("test_room")!.userRoomCollection)
    }

    let oneToOneRoomJson = [
        "url": "/trevorah-test5",
        "userCount": 2,
        "mentions": 0,
        "roomMember": true,
        "avatarUrl": "https://avatars-01.gitter.im/gh/uv/3/trevorah-test5",
        "unreadItems": 0,
        "lastAccessTime": "2016-09-14T12:53:17.587Z",
        "name": "trevorah-test5",
        "githubType": "ONETOONE",
        "id": "one_to_one_room",
        "lurk": false,
        "user": [
            "avatarUrl": "https://avatars-01.gitter.im/gh/uv/3/test_user",
            "avatarUrlMedium": "https://avatars0.githubusercontent.com/u/13469832?v=3&s=128",
            "avatarUrlSmall": "https://avatars0.githubusercontent.com/u/13469832?v=3&s=60",
            "displayName": "test_user",
            "gv": 3,
            "id": "test_user",
            "url": "/test_user",
            "username": "test_user",
        ],
        "oneToOne": true,
        "public": false,
        "noindex": false
    ] as [String : Any]

    func testOneToOneRoomHasUserAttached() throws {
        try jsonToDatabase!.upsertRoom(oneToOneRoomJson)

        let room = getRoomFromDb("one_to_one_room")
        XCTAssertEqual(room?.user?.id, "test_user")
    }

    private let test_group_json = [
        "id": "test_group",
        "name": "Test Group",
        "uri":"test-group",
        "backedBy": [
            "type": "GH_ORG",
            "linkPath": "test-org"
        ],
        "avatarUrl": "https://avatars.gitter.im/group/i/test_group"
    ] as [String : Any]

    private let test_group_room_json = [
        "id": "test_group_room",
        "groupId": "test_group",
        "security": "PUBLIC",
        "avatarUrl": "https://avatars-01.gitter.im/group/i/57542c12c43b8c601976fa66",
        "userCount": 3,
        "mentions": 0,
        "activity": false,
        "uri": "trevorah-test/test-repo",
        "roomMember": true,
        "topic": "i am a test repo",
        "unreadItems": 0,
        "lastAccessTime": "2016-05-17T19:52:43.976Z",
        "name": "trevorah-test/test-repo",
        "tags": ["test"],
        "githubType": "REPO",
        "lurk": true,
        "oneToOne": false,
        "noindex": false,
        "url": "/trevorah-test/test-repo"
    ] as [String : Any]

    private let room_with_group_json = [
        "id": "room_with_group",
        "name": "gitterHQ/gitter",
        "topic": "Please look at our support site before asking questions. There are a lot of frequently asked questions there. http://support.gitter.im",
        "avatarUrl": "https://avatars-01.gitter.im/group/i/57542c12c43b8c601976fa66",
        "uri": "gitterHQ/gitter",
        "oneToOne": false,
        "userCount": 4139,
        "unreadItems": 0,
        "mentions": 0,
        "lastAccessTime": "2016-08-01T09:22:54.474Z",
        "favourite": 15,
        "lurk": true,
        "activity": true,
        "url": "/gitterHQ/gitter",
        "githubType": "REPO",
        "security": "PUBLIC",
        "premium": true,
        "noindex": false,
        "tags": [
            "questions",
            "features",
            "gitter",
            "hangout",
            "featured:gitter"
        ],
        "permissions": [
            "admin": true
        ],
        "roomMember": true,
        "groupId": "group_within_room",
        "group": [
            "id": "group_within_room",
            "name": "gitterHQ",
            "uri": "gitterHQ",
            "backedBy": [
                "type": "GH_ORG",
                "linkPath": "gitterHQ"
            ],
            "avatarUrl": "https://avatars-01.gitter.im/group/i/57542c12c43b8c601976fa66"
        ],
        "backend": [
            "type": "GH_REPO",
            "linkPath": "gitterHQ/gitter"
        ],
        "public": true,
        "v": 14780
    ] as [String : Any]

    func testExistingGroupIsAddedToNewRoom() throws {
        try jsonToDatabase!.upsertGroup(test_group_json)

        try jsonToDatabase!.upsertRoom(test_group_room_json)

        let room = getRoomFromDb("test_group_room")
        XCTAssertEqual("test_group", room!.group!.id)
    }

    func testNewRoomIsAddedToExistingGroup() throws {
        try jsonToDatabase!.upsertGroup(test_group_json)

        try jsonToDatabase!.upsertRoom(test_group_room_json)

        let group = getGroupFromDb("test_group")
        XCTAssertEqual("test_group_room", group!.rooms.first!.id)
    }

    func testNewGroupIsAddedToExistingRoom() throws {
        try jsonToDatabase!.upsertRoom(test_group_room_json)

        try jsonToDatabase!.upsertGroup(test_group_json)

        let room = getRoomFromDb("test_group_room")
        XCTAssertEqual("test_group", room!.group!.id)
    }

    func testExistingRoomIsAddedToNewGroup() throws {
        try jsonToDatabase!.upsertRoom(test_group_room_json)

        try jsonToDatabase!.upsertGroup(test_group_json)

        let group = getGroupFromDb("test_group")
        XCTAssertEqual("test_group_room", group!.rooms.first!.id)
    }

    func testExistingGroupIsAddedToExistingRoom() throws {
        try jsonToDatabase!.upsertRoom(room_with_group_json)

        let room = getRoomFromDb("room_with_group")
        XCTAssertEqual("group_within_room", room!.group!.id)
    }

    func testExistingRoomIsAddedToExistingGroup() throws {
        try jsonToDatabase!.upsertRoom(room_with_group_json)

        let group = getGroupFromDb("group_within_room")
        XCTAssertEqual("room_with_group", group!.rooms.first!.id)
    }

    private let suggestions_json: [JsonObject] = [
        [
            "id": "suggestion",
            "uri": "suggestion/suggestion",
            "avatarUrl": "https://avatars2.githubusercontent.com/suggestion?&s=48",
            "githubType": "REPO",
            "name": "suggestion/suggestion",
            "exists": true,
            "security": "PUBLIC",
            "description": "Suggestions",
            "v": 789,
            "tags": [],
            "userCount": 1312,
            "groupId": "57542d06c43b8c6019778d8c",
            "topic": "Suggestions",
            "url": "/suggestion/suggestion",
            "public": true
        ]
    ]

    func testSuggestionsCanBeUpdated() throws {
        try jsonToDatabase!.updateSuggestions(suggestions_json)

        XCTAssertEqual("suggestion", try getSuggestionCollection().suggestions.first!.id)
    }

    private func getRoomFromDb(_ id: String) -> Room? {
        let fetchRequest = NSFetchRequest<Room>()
        fetchRequest.entity = NSEntityDescription.entity(forEntityName: "Room", in: managedObjectContext!)
        fetchRequest.predicate = NSPredicate(format: "id == %@", id)
        let result = try! managedObjectContext!.fetch(fetchRequest)

        return result.last
    }

    private func getGroupFromDb(_ id: String) -> Group? {
        let fetchRequest = NSFetchRequest<Group>()
        fetchRequest.entity = NSEntityDescription.entity(forEntityName: "Group", in: managedObjectContext!)
        fetchRequest.predicate = NSPredicate(format: "id == %@", id)
        let result = try! managedObjectContext!.fetch(fetchRequest)

        return result.last
    }

    private func getSortedRoomList() -> [Room] {
        let fetchRequest = NSFetchRequest<Room>()
        fetchRequest.entity = NSEntityDescription.entity(forEntityName: "Room", in: managedObjectContext!)
        fetchRequest.predicate = NSPredicate(format: "userRoomCollection != NULL")
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "favourite_comparable", ascending: true)]

        return try! managedObjectContext!.fetch(fetchRequest)
    }

    private func getSuggestionCollection() throws -> UserSuggestionCollection {
        let fetchRequest = NSFetchRequest<UserSuggestionCollection>()
        fetchRequest.entity = NSEntityDescription.entity(forEntityName: "UserSuggestionCollection", in: managedObjectContext!)
        fetchRequest.fetchLimit = 1

        return try managedObjectContext!.fetch(fetchRequest).first!
    }
}
