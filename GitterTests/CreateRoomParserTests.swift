import Foundation
import XCTest
import CoreData
@testable import Gitter

class CreateRoomParserTests: XCTestCase {

    private var community: Group?
    private let parser = CreateRoomParser()

    override func setUp() {
        super.setUp()

        let modelURL = Bundle.main.url(forResource: "Gitter", withExtension: "momd")!
        let model = NSManagedObjectModel(contentsOf: modelURL)!
        let coordinator = NSPersistentStoreCoordinator(managedObjectModel: model)
        try! coordinator.addPersistentStore(ofType: NSInMemoryStoreType, configurationName: nil, at: nil, options: nil)

        let managedObjectContext = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
        managedObjectContext.persistentStoreCoordinator = coordinator
        let entity = NSEntityDescription.entity(forEntityName: "Group", in: managedObjectContext)
        let group = NSManagedObject(entity: entity!, insertInto: managedObjectContext) as! Group
        group.id = "X"
        group.name = "X"
        group.uri = "X"
        group.avatarUrl = "http://x.com/x.png"
        community = group

    }

    func testErrorIsThrownIfCommunityIsMissing() {
        XCTAssertThrowsError(try parser.parse(community: nil, roomName: nil, linkedRepo: nil, isPrivate: false, orgCanJoin: false, githubOnly: false, addBadge: false))
    }

    func testErrorIsThrownIfRoomIsMissing() {
        XCTAssertThrowsError(try parser.parse(community: community, roomName: nil, linkedRepo: nil, isPrivate: false, orgCanJoin: false, githubOnly: false, addBadge: false))
    }

    func testSimpleRoomCreation() throws {
        var result = try parser.parse(community: community, roomName: "cat", linkedRepo: nil, isPrivate: false, orgCanJoin: false, githubOnly: false, addBadge: false)
        XCTAssertEqual(result["name"] as? String, "cat")
    }

    func testErrorIsThrownIfRoomNameHasSpaces() {
        XCTAssertThrowsError(try parser.parse(community: community, roomName: "cat dog", linkedRepo: nil, isPrivate: false, orgCanJoin: false, githubOnly: false, addBadge: false))
    }

    func testRoomNameAllowsDashes() throws {
        var result = try parser.parse(community: community, roomName: "cat-dog", linkedRepo: nil, isPrivate: false, orgCanJoin: false, githubOnly: false, addBadge: false)
        XCTAssertEqual(result["name"] as? String, "cat-dog")
    }

    func testErrorIsThrownIfRoomNameHasPuctuation() {
        XCTAssertThrowsError(try parser.parse(community: community, roomName: "cat.dog", linkedRepo: nil, isPrivate: false, orgCanJoin: false, githubOnly: false, addBadge: false))
    }
}
