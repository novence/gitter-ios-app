import Foundation

struct TextUtils {

    static func getClosestWord(_ text: String, underCursor range: NSRange) -> (word: String, range: NSRange)? {
        if let swiftRange = convertNSRange(range, inString: text) {
            var start = swiftRange.lowerBound
            var end = swiftRange.upperBound

            while start > text.startIndex && !isWhitespace(text, position: text.characters.index(before: start)) {
                start = text.characters.index(before: start)
            }

            while end <= text.endIndex && !isWhitespace(text, position: end) {
                end = text.characters.index(after: end)
            }

            let wordRange = start ..< end
            let word = text.substring(with: wordRange)

            guard isSingleWord(word) else {
                return nil
            }

            let nsWordRange = convertRange(wordRange, inString: text)

            return (word, nsWordRange)
        }

        return nil
    }

    private static func isWhitespace(_ text: String, position: String.CharacterView.Index) -> Bool {
        let characters = text.characters

        if characters.endIndex <= position || characters.startIndex > position {
            return true
        }

        let substring = String(characters[position])
        let whitespaceSet = CharacterSet.whitespacesAndNewlines

        return substring.rangeOfCharacter(from: whitespaceSet) != nil
    }

    private static func isSingleWord(_ string: String) -> Bool {
        let whitespaceSet = CharacterSet.whitespacesAndNewlines

        return string.characters.count > 0 && string.rangeOfCharacter(from: whitespaceSet) == nil
    }

    private static func convertNSRange(_ range: NSRange, inString string: String) -> Range<String.Index>? {
        let utf16view = string.utf16
        let from16 = utf16view.index(utf16view.startIndex, offsetBy: range.location, limitedBy: utf16view.endIndex) ?? utf16view.endIndex
        let to16 = utf16view.index(from16, offsetBy: range.length, limitedBy: utf16view.endIndex) ?? utf16view.endIndex
        if let from = String.Index(from16, within: string),
            let to = String.Index(to16, within: string) {
            return from ..< to
        }
        return nil
    }

    private static func convertRange(_ range: Range<String.Index>, inString string: String) -> NSRange {
        let utf16view = string.utf16
        let from = String.UTF16View.Index(range.lowerBound, within: utf16view)
        let to = String.UTF16View.Index(range.upperBound, within: utf16view)
        return NSMakeRange(utf16view.distance(from: utf16view.startIndex, to: from), utf16view.distance(from: from, to: to))
    }
}
