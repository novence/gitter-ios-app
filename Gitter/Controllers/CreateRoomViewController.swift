import Foundation
import UIKit

class CreateRoomViewController: UITableViewController, PickCommunityViewControllerDelegate, PickRoomNameViewControllerDelegate {

    @IBOutlet var communityLabel: UILabel!
    @IBOutlet var roomNameLabel: UILabel!
    @IBOutlet var orgMembersLabel: UILabel!

    @IBOutlet var createButton: UIBarButtonItem!
    @IBOutlet var publicPrivateControl: UISegmentedControl!
    @IBOutlet var orgCanJoinSwitch: UISwitch!
    @IBOutlet var onlyGithubSwitch: UISwitch!
    @IBOutlet var addBadgeSwitch: UISwitch!

    var delegate: CreateRoomViewControllerDelegate?

    private let NAME_SECTION = 0
    private let PERMISSIONS_SECTION = 1
    private let ORG_SECTION = 2
    private let GITHUB_ONLY_SECTION = 3
    private let BADGE_SECTION = 4
    private let PUBLIC_SEGMENT = 0
    private let PRIVATE_SEGMENT = 1

    private let parser = CreateRoomParser()

    private var community: Group? {
        didSet {
            if let linkedRepo = linkedRepo, let community = community {
                if linkedRepo.owner != community.githubGroupName {
                    // keep the community but wipe the room name to stay valid
                    self.linkedRepo = nil
                    self.roomName = nil
                }
            }

            if !orgSectionEnabled {
                orgCanJoin = false
            }

            render()
        }
    }
    private var roomName: String? {
        didSet {
            render()
        }
    }
    private var linkedRepo: Repo? {
        didSet {
            if let linkedRepo = linkedRepo {
                isPrivate = linkedRepo.isPrivate
            }
            if !githubOnlySectionEnabled {
                githubOnly = false
            }
            if !addBadgeSectionEnabled {
                addBadge = false
            }
            render()
        }
    }
    private var isPrivate = false {
        didSet {
            if !orgSectionEnabled {
                orgCanJoin = false
            }
            if !githubOnlySectionEnabled {
                githubOnly = false
            }
            render()
        }
    }
    private var orgCanJoin = false {
        didSet {
            render()
        }
    }
    private var githubOnly = false {
        didSet {
            render()
        }
    }
    private var addBadge = false {
        didSet {
            render()
        }
    }
    private var privateOptionEnabled: Bool {
        get {
            return linkedRepo?.isPrivate ?? true
        }
    }
    private var publicOptionEnabled: Bool {
        get {
            return !(linkedRepo?.isPrivate ?? false)
        }
    }
    private var orgSectionEnabled: Bool {
        get {
            return isPrivate && community?.backedByType == .GithubOrg
        }
    }
    private var githubOnlySectionEnabled: Bool {
        get {
            if let backedByType = community?.backedByType {
                let isCommunityBackedByGithub = backedByType == .GithubOrg || backedByType == .GithubRepo || backedByType == .GithubUser
                return !isPrivate && isCommunityBackedByGithub
            } else {
                return false
            }
        }
    }
    private var addBadgeSectionEnabled: Bool {
        get {
            if let linkedRepo = linkedRepo {
                return !linkedRepo.isPrivate
            } else {
                return false
            }
        }
    }

    @IBAction func onCancelButtonTapped(_ sender: UIBarButtonItem) {
        dismiss(animated: true, completion: nil)
    }

    @IBAction func didTapCreateRoom(_ sender: UIBarButtonItem) {

        do {
            let body = try parser.parse(community: community,
                                        roomName: roomName,
                                        linkedRepo: linkedRepo,
                                        isPrivate: isPrivate,
                                        orgCanJoin: orgCanJoin,
                                        githubOnly: githubOnly,
                                        addBadge: addBadge)

            createButton.isEnabled = false

            RestService().createRoom(body, inGroup: community!, completionHandler: { (error, id) in
                guard let roomId = id , error == nil else {
                    LogError("failed to create room", error: error!)
                    self.showError("Failed to create room, please try again")
                    return
                }

                self.dismiss(animated: true) {
                    self.delegate?.didCreateRoom(self, roomId: roomId)
                }
            })
        } catch CreateRoomParserError.communityMissing {
            showError("Please pick a Community")
        } catch CreateRoomParserError.roomNameMissing {
            showError("Please choose a name for your room")
        } catch CreateRoomParserError.roomNameInvalid {
            showError("Please change your room name to only include letters, numbers and dashes")
        } catch {
            LogError("Impossible create room validation error", error: error)
        }
    }

    @IBAction func didTogglePublicPrivate(_ sender: UISegmentedControl) {
        isPrivate = sender.selectedSegmentIndex == 1
    }
    @IBAction func didToggleOrgCanJoin(_ sender: UISwitch) {
        orgCanJoin = sender.isOn
    }
    @IBAction func didToggleOnlyGithub(_ sender: AnyObject) {
        githubOnly = sender.isOn
    }
    @IBAction func didToggleAddBadge(_ sender: AnyObject) {
        addBadge = sender.isOn
    }

    override func tableView(_ tableView: UITableView, titleForFooterInSection section: Int) -> String? {
        switch section {
        case NAME_SECTION:
            let communityName = community?.uri ?? "…"
            let room = roomName ?? "…"
            var text = "Your room will be at gitter.im/\(communityName)/\(room)"
            if let linkedRepo = linkedRepo {
                text += " and linked with \(linkedRepo.uri) on GitHub."
            }
            return text
        case ORG_SECTION:
            return orgSectionEnabled ? super.tableView(tableView, titleForFooterInSection: section) : nil
        case BADGE_SECTION:
            return addBadgeSectionEnabled ? super.tableView(tableView, titleForFooterInSection: section) : nil
        default:
            return super.tableView(tableView, titleForFooterInSection: section)
        }
    }

    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        let smallEnoughToHide = CGFloat(0.1)

        switch section {
        case ORG_SECTION:
            return orgSectionEnabled ? super.tableView(tableView, heightForHeaderInSection: section) : smallEnoughToHide
        case GITHUB_ONLY_SECTION:
            return githubOnlySectionEnabled ? super.tableView(tableView, heightForHeaderInSection: section) : smallEnoughToHide
        case BADGE_SECTION:
            return addBadgeSectionEnabled ? super.tableView(tableView, heightForHeaderInSection: section) : smallEnoughToHide
        default:
            return super.tableView(tableView, heightForHeaderInSection: section)
        }
    }

    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        let smallEnoughToHide = CGFloat(0.1)

        switch section {
        case ORG_SECTION:
            return orgSectionEnabled ? super.tableView(tableView, heightForFooterInSection: section) : smallEnoughToHide
        case GITHUB_ONLY_SECTION:
            return githubOnlySectionEnabled ? super.tableView(tableView, heightForFooterInSection: section) : smallEnoughToHide
        case BADGE_SECTION:
            return addBadgeSectionEnabled ? super.tableView(tableView, heightForFooterInSection: section) : smallEnoughToHide
        default:
            return super.tableView(tableView, heightForFooterInSection: section)
        }
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? PickCommunityViewController {
            vc.community = community
            vc.delegate = self
        } else if let vc = segue.destination as? PickRoomNameViewController {
            vc.name = roomName
            vc.linkedRepo = linkedRepo
            vc.community = community
            vc.delegate = self
        }
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case ORG_SECTION:
            return orgSectionEnabled ? super.tableView(tableView, numberOfRowsInSection: section): 0
        case GITHUB_ONLY_SECTION:
            return githubOnlySectionEnabled ? super.tableView(tableView, numberOfRowsInSection: section): 0
        case BADGE_SECTION:
            return addBadgeSectionEnabled ? super.tableView(tableView, numberOfRowsInSection: section): 0
        default:
            return super.tableView(tableView, numberOfRowsInSection: section)
        }
    }

    func didChangeCommunity(_ pickCommunityViewController: PickCommunityViewController, community: Group?) {
        self.community = community
    }

    func didChangeName(_ pickRoomNameViewController: PickRoomNameViewController, name: String?, repo: Repo?) {
        self.roomName = name
        self.linkedRepo = repo
    }

    func didIntendToCreateNewCommunity(_ pickCommunityViewController: PickCommunityViewController) {
        dismiss(animated: true) {
            self.delegate?.didIntendToCreateCommunity(self)
        }
    }

    private func render() {
        tableView.reloadData()
        roomNameLabel.text = roomName ?? "Required"
        communityLabel.text = community?.name ?? "Required"
        publicPrivateControl.selectedSegmentIndex = isPrivate ? PRIVATE_SEGMENT : PUBLIC_SEGMENT
        publicPrivateControl.setEnabled(publicOptionEnabled, forSegmentAt: PUBLIC_SEGMENT)
        publicPrivateControl.setEnabled(privateOptionEnabled, forSegmentAt: PRIVATE_SEGMENT)
        if let org = community?.backedBy_linkPath , community?.backedByType == .GithubOrg {
            orgMembersLabel.text = "Members of \(org) can join"
        }
        orgCanJoinSwitch.isOn = orgCanJoin
        onlyGithubSwitch.isOn = githubOnly
        addBadgeSwitch.isOn = addBadge
    }

    private func showError(_ message: String) {
        let alert = UIAlertController(title: message, message: nil, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        present(alert, animated: true, completion: nil)
    }
}

protocol CreateRoomViewControllerDelegate {
    func didIntendToCreateCommunity(_ createRoomViewController: CreateRoomViewController)
    func didCreateRoom(_ createRoomViewController: CreateRoomViewController, roomId: String)
}
