import Foundation
import CoreData

class PeopleViewController: UITableViewController, NSFetchedResultsControllerDelegate, HintViewDelegate {

    private var fetchedResultsController: NSFetchedResultsController<Room>?
    private var hintView: HintView?

    override func loadView() {
        super.loadView()

        let managedObjectContext = CoreDataSingleton.sharedInstance.uiManagedObjectContext

        let roomFetchRequest = NSFetchRequest<Room>(entityName: "Room")
        roomFetchRequest.predicate = NSPredicate(format: "oneToOne == YES")
        roomFetchRequest.sortDescriptors = [NSSortDescriptor(key: "favourite_comparable", ascending: true),
                                            NSSortDescriptor(key: "mentions", ascending: false),
                                            NSSortDescriptor(key: "unreadItems", ascending: false),
                                            NSSortDescriptor(key: "lastAccessTime", ascending:false)]
        fetchedResultsController = NSFetchedResultsController(fetchRequest: roomFetchRequest, managedObjectContext: managedObjectContext, sectionNameKeyPath: nil, cacheName: nil)
        fetchedResultsController?.delegate = self

        hintView = Bundle.main.loadNibNamed("HintView", owner: self, options: nil)!.first as? HintView
        hintView?.button.setTitle("👩👱❓ 🔍❗️", for: UIControlState())
        hintView?.delegate = self

        try! fetchedResultsController?.performFetch()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(UINib(nibName: "AvatarWithBadgeCell", bundle: nil), forCellReuseIdentifier: "AvatarWithBadgeCell")
        tableView.tableFooterView = UIView()
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let numberOfRows = fetchedResultsController?.sections?[section].numberOfObjects ?? 0
        tableView.backgroundView = numberOfRows == 0 ? hintView : nil
        return numberOfRows
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AvatarWithBadgeCell", for: indexPath) as! AvatarWithBadgeCell
        configureCell(cell, indexPath: indexPath)
        return cell
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let room = fetchedResultsController!.object(at: indexPath)
        let rootViewController = splitViewController as! RootViewController

        rootViewController.navigateTo(roomId: room.id, sender: self)
    }

    func didTapButton(_ hintView: HintView, button: UIButton) {
        (tabBarController as! GitterTabBarController).switchTabs(.search)
    }

    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.beginUpdates()
    }

    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        TableViewChangeHandler.applyRowChange(to: tableView, at: indexPath, for: type, newIndexPath: newIndexPath)
    }

    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.endUpdates()
    }

    private func configureCell(_ cell: AvatarWithBadgeCell, indexPath: IndexPath) {
        let room = fetchedResultsController!.object(at: indexPath)

        AvatarUtils.sharedInstance.configure(cell.avatar, room: room, size: AvatarWithBadgeCell.avatarHeight)
        cell.mainLabel.text = room.name
        cell.sideLabel.text = "@" + room.uri
        cell.bottomLabel.text = nil
        if (room.intMentions > 0) {
            cell.badgeType = .mention
            cell.badgeLabel.text = "@"
        } else if (room.intUnreadItems > 0) {
            cell.badgeType = .unread
            cell.badgeLabel.text = room.intUnreadItems > 99 ? "99+" : String(room.intUnreadItems)
        } else if (room.activity && room.lurk) {
            cell.badgeType = .activityIndicator
            cell.badgeLabel.text = nil
        } else {
            cell.badgeType = .none
            cell.badgeLabel.text = nil
        }
    }
}
