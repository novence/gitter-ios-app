import Foundation
import CoreData

class UserSuggestionCollection: NSManagedObject {
    @NSManaged var suggestions: Set<Room>
}
