import Foundation
import CoreData

class UserGroupCollection: NSManagedObject {
    @NSManaged var groups: Set<Group>
}
