import Crittercism

class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, willFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        Crittercism.enable(withAppID: "539f10fd0729df2353000001")

        // remove old cruft (remove after June 2016)
        clearLegacyUserAgentString()
        BetaCleaner.clean()

        // start up legacy singletons
        TRAppSettings.sharedInstance()
        TRNotificationsManager.sharedInstance().start()
        NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: UserIdChanged), object: nil, queue: nil) { (note) in
            let userId = note.object as! String
            Crittercism.setUsername(userId)
            Crittercism.setValue(userId, forKey: "userId")
        }
        // The singleton event controller lib will revoke tokens. We cant stop it, but we can at least clean up after it.
        NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: AuthTokenRevoked), object: nil, queue: nil) { (note) in
            print("Legacy code has started logging out. Cleaning up.")
            self.logout()
            let storyboard = UIStoryboard(name: "Login", bundle: Bundle.main)
            self.window?.rootViewController = storyboard.instantiateInitialViewController()
        }

        // prep for mounting root view controller
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.makeKeyAndVisible()

        if (hasLogoutBeenRequested()) {
            logout()
        }

        if (LoginData().isLoggedIn()) {
            let storyboard = UIStoryboard(name: "MainStoryboard", bundle: Bundle.main)
            window?.rootViewController = storyboard.instantiateInitialViewController()
        } else {
            let storyboard = UIStoryboard(name: "Login", bundle: Bundle.main)
            self.window?.rootViewController = storyboard.instantiateInitialViewController()
        }

        return true
    }

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        application.setMinimumBackgroundFetchInterval(UIApplicationBackgroundFetchIntervalMinimum)
        return true
    }

    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        // user has tapped on a notification
        if let id = getRoomIdFromNotificationPayload(userInfo) {
            NotificationCenter.default.post(name: Notification.Name(rawValue: "GitterPushNotification"), object: nil, userInfo: ["id": id])
        }
    }

    private func clearLegacyUserAgentString() {
        UserDefaults.standard.removeObject(forKey: "UserAgent")
    }

    private func hasLogoutBeenRequested() -> Bool {
        return UserDefaults.standard.bool(forKey: "Signout")
    }

    private func logout() {
        TRAuthController.sharedInstance().signOut()
        LoginData().clear()
        URLCache.shared.removeAllCachedResponses()
        CoreDataSingleton.sharedInstance.deleteAllObjects { (error, didSaveNewData) in
            LogIfError("failed to delete all objects", error: error)
        }
        UserDefaults.standard.set(false, forKey: "Signout")
    }

    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        guard LoginData().isLoggedIn() else {
            return completionHandler(.noData)
        }

        if let roomId = getRoomIdFromNotificationPayload(userInfo) {
            RestService().requestRoom(roomId, completionHandler: { (error, didSaveNewData) in
                guard error == nil else {
                    LogError("failed to background fetch room data", error: error!)
                    completionHandler(.failed)
                    return
                }
                NSLog("background fetched room data: %@, new data: %@", roomId, String(didSaveNewData))
                completionHandler(didSaveNewData ? .newData : .noData)
            })
        } else {
            completionHandler(.noData)
        }
    }

    func application(_ application: UIApplication, handleActionWithIdentifier identifier: String?, forRemoteNotification userInfo: [AnyHashable : Any], withResponseInfo responseInfo: [AnyHashable : Any], completionHandler: @escaping () -> Void) {

        guard identifier == "CHAT_REPLY" else {
            return completionHandler()
        }

        guard LoginData().isLoggedIn() else {
            return completionHandler()
        }

        if let roomId = getRoomIdFromNotificationPayload(userInfo), let reply = responseInfo[UIUserNotificationActionResponseTypedTextKey] as? String {
            RestService().send(message: reply, toRoomId: roomId, completionHandler: { (error, didSaveNewData) in
                LogIfError("failed to reply from push notification", error: error)
                completionHandler()
            })
        } else {
            completionHandler()
        }
    }

    func application(_ application: UIApplication, performFetchWithCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        guard LoginData().isLoggedIn() else {
            return completionHandler(.noData)
        }

        RestService().getUserRooms { (error, didSaveNewData) in
            guard error == nil else {
                LogError("failed to background fetch user rooms", error: error!)
                completionHandler(.failed)
                return
            }

            NSLog("background fetched user rooms data. new data: %@", String(didSaveNewData))
            completionHandler(didSaveNewData ? .newData : .noData)
        }
    }

    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        NSLog("didRegisterForRemoteNotificationsWithDeviceToken: %@", deviceToken as NSData)
        let uniqueDeviceId = TRAppSettings.sharedInstance().uniqueDeviceId()
        TRNotificationsManager.sharedInstance().registerDevice(forNotifications: deviceToken, forUniqueDeviceId: uniqueDeviceId)
    }

    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        NSLog("didFailToRegisterForRemoteNotificationsWithError: %@", error as NSError)
    }

    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any]) -> Bool {
        return false
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        if (hasLogoutBeenRequested()) {
            logout()
            let storyboard = UIStoryboard(name: "Login", bundle: Bundle.main)
            self.window?.rootViewController = storyboard.instantiateInitialViewController()
        }
    }

    private func getRoomIdFromNotificationPayload(_ payload:[AnyHashable: Any]?) -> String? {
        if let payload = payload {
            if let url = payload["l"] as? String {
                // hangover from the appcache days
                // l(ink) would be in the form "/mobile/chat#1234"
                let id = url.components(separatedBy: "#")[1]
                return id
            } else if let id = payload["i"] as? String {
                // just in case we drop support for the old links
                return id
            } else if let id = payload["id"] as? String {
                // just in case we decide to spend an extra character on the id key
                return id
            } else {
                return nil
            }
        } else {
            return nil
        }
    }
}
